// VierGewinntV2.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include <stdlib.h>


int spielfeld[7][6];   //Spalte , Zeile  //speicher frei machen f�r ein array 2 dim
bool fertig = false;   //bo�l kann true und false annehmen 

void erstelleSpielfeld() {      //methode erstellt spielfeld gibt nichts zurueck 

	groesse(7, 7);           //Gr��e Spalte plus 1 Zeile f�r "Werfen"

	int x; //spalte
	int y; //zeile

	for (x = 0; x < 7; x++) {       //Spaltendurchlauf
		for (y = 0; y < 7; y++) {   //Zeilendurchlauf

			spielfeld[x][y] = 0;    //Felder 0 setzen 
		}
	}

	for (int i = 42; i < 49; i++) {  //F�rbt Buttons zum klicken
		farbe(i, WHITE);
	}
}


void reinwerfen(int x, int spieler) {   //nur x weil man nur von oben steine reinwerfen kann (Spalten)
	int zaehlerY = 5;       /// 6 Zeilen runterfallen 
	while (spielfeld[x][zaehlerY - 1] == 0 && zaehlerY - 1 >= 0) { //�berpruft: Solange kein Stein im Weg ist "f�llt" es runter
		zaehlerY--;  //damit es runterz�hlt 
	}
	                             

	if (spieler==1 && spielfeld[x] [zaehlerY]==0) {  //spieler 1 rot  //nur anmalen wenn es  0 ist ( NICHT �berschreiben)

		farbe((x + (zaehlerY * 7)), RED);      //Koordinaten Formel 
		
	}

	else if (spieler == 2 && spielfeld[x][zaehlerY] == 0) {  //spieler 2 gelb 


		farbe((x + (zaehlerY * 7)), YELLOW);



	}
	spielfeld[x][zaehlerY] = spieler; //Feld bekommt eine Nummer / Wert  ist dann  besetzt mit einem Stein //spieler variable
}
//---------------------------------------------Regeln--------------------------------------------------------------------

void ueberpruefung() {  //

	int x;
	int y;
	int wert = 0;          // ist da um punkt zu speichern 1 oder 2 
	int zaehler = 1;       // damit die gewinnfolge mit 1 beginnt 

						   //vertikal

	for (x = 0; x < 7; x++) {   //Durchlauf entlag der Spalte 
		for (y = 5; y >= 0; y--) { //Durchlauf entlag der Zeile 
			if (spielfeld[x][y] != 0) {  //wenn spielfeld ungleich 0 ist dann 
				if (spielfeld[x][y] == wert) { //vergleich wert und spielfeld was drinne gespeichert ist 
					zaehler++; //z�hlt die gleichen Werte (1111)
				}
				else {
					zaehler = 1;    //zurueckgesetzt 
					wert = 0;
				}
				wert = spielfeld[x][y];  //wert speichert immer die zahl davor
			}
			if (zaehler == 4) {     //gewonnen
				fertig = true;
				break;
			}

		}
		zaehler = 1;   //werte werden zur�ckgesetzt f�r die n�chste �berpr�fung 
		wert = 0; 
	}
	//horizontal 

	for (y = 0; y < 6; y++) {
		for (x = 0; x < 7; x++) {

			if (spielfeld[x][y] != 0) {  //wenn spielfeld ungleich 0 ist dann 
				if (spielfeld[x][y] == wert) { //vergleich wert und spielfeld was drinne gespeichert ist 
					zaehler++;   //z�hlt die gleichen Werte (1111)
				}
				else {
					zaehler = 1;  //zur�ckgesetzt
				}
				wert = spielfeld[x][y];  //wert speichert immer die zahl davor
				
				
			}
			else {
				zaehler = 1;
				wert = 0;
			}
			if (zaehler == 4) {
				fertig = true;
				
				break;

				
			}

		}
		zaehler = 1;
		wert = 0;
	}



	//diagonal 1( unten)
	for (int i = 3; i < 7; i++) {    //beginnt bei 3 bis 6
		x = i;                       //startwert zuweisen 
		y = 0;                       //zeile 0
		for (int j = i; j >= 0; j--) { //j symbolisiert x achse 

			if (spielfeld[x][y] != 0) {  //wenn spielfeld ungleich 0 ist dann
				if (spielfeld[x][y] == wert) { //vergleich wert und spielfeld was drinne gespeichert ist 
					zaehler++;                //z�hlt die gleichen Werte (1111)
				}
				else {
					zaehler = 1;
					wert = 0;
				}
				wert = spielfeld[x][y];   //wert speichert immer die zahl davor
			}
			else {
				zaehler = 1;
				wert = 0;
			}
			if (zaehler == 4) {
				fertig = true;
				break;
			}


			x = x - 1;   //einer schritt nach links
			y = y + 1;   // 1 hoch und anmalen
		}
	}





	//diagonal 1.1 (oben)

	for (int i = 3; i >= 1; i--) {   //"obere" x achse 
		x = i;
		y = 5;                       //zeile
		for (int j = i; j < 7; j++) {   //j symbolisiert x achse 

			if (spielfeld[x][y] != 0) {  //wenn spielfeld ungleich 0 ist dann
				if (spielfeld[x][y] == wert) { //vergleich wert und spielfeld was drinne gespeichert ist 
					zaehler++;                //z�hlt die gleichen Werte (1111)
				}
				else {
					zaehler = 1;
					wert = 0;
					
				}
				wert = spielfeld[x][y];  //wert speichert immer die zahl davor
			}
			else {
				zaehler = 1;
				wert = 0;
			}
			if (zaehler == 4) {
				fertig = true;
				break;
			}


			x = x + 1;   //einer schritt nach rechts
			y = y - 1;   // 1 unten anmalen
		}
	}





	//diagonale 2 (unten)

	for (int i = 3; i >= 1; i--) {
		x = i;                       //startwert zuweisen 
		y = 0;                       //zeile 0
		for (int j = i; j < 7; j++) {  //j symbolisiert x achse 
			
			if (spielfeld[x][y] != 0) {  //wenn spielfeld ungleich 0 ist da 
				if (spielfeld[x][y] == wert) { //vergleich wert und spielfeld was drinne gespeichert ist 
					zaehler++;                //z�hlt die gleichen Werte (1111)
				}
				else {
					zaehler = 1;
					wert = 0;
					
				}
				wert = spielfeld[x][y]; //wert speichert immer die zahl davor
			}
			else {
				zaehler = 1;
				wert = 0;
			}
			if (zaehler == 4) {
				fertig = true;
				break;
			}


			x = x + 1;   //einer schritt nach rechts
			y = y + 1;   // 1 hoch und anmalen
		}
	}

	//diagonale 2.1 (oben)

	for (int i = 3; i < 6; i++) {   //"obere" x achse 
		x = i;
		y = 5;
		for (int j = i; j >= 0; j--) { //j symbolisiert x achse 
			if (spielfeld[x][y] != 0) {  //wenn spielfeld ungleich 0 ist dann
				if (spielfeld[x][y] == wert) { //vergleich wert und spielfeld was drinne gespeichert ist 
					zaehler++;                //z�hlt die gleichen Werte (1111)
				}
				else {
					zaehler = 1;
				}
				wert = spielfeld[x][y];  //wert speichert immer die zahl davor
			}
			else {
				zaehler = 1;
				wert = 0;
			}
			if (zaehler == 4) {
				fertig = true;
				break;
			}


			x = x - 1;   //einer schritt nach links
			y = y - 1;   // 1 unten  anmalen
		}
	}

}
//---------------------------------------------------ende regeln---------------------------------------------
int main() {
	
	erstelleSpielfeld();

	int spieler = rand() % 2 + 1;        //damit ein spieler zuf�llig ausgesucht wird
	int spalte;                          // speichert die ausgew�hlte Spalte 
	while (fertig == false) {            //solange keiner gewonnen hat 
		char *a = abfragen();            //empfngt eine Nachricht von BOS -> Knopf der gefr�ckt wurde
		if (strlen(a) > 0) {      //Wenn die enthaltene Nachricht l�nger als 0 ist, wird weiter �berpr�ft.
			if (a[0] == '#') {       //f�ngt die nachricht mit # handelt es sich um ein knopfdruck. 
				
				sscanf_s(a, "# %d", &spalte);  //Lese den gedr�ckten Knopf aus der Nachricht aus.

				if (spalte >= 42 && spalte <= 48 &&spielfeld [spalte -42] [5] ==0) {   //solange 5. zeile der ausgew�hlten Spalte leer ist darf man in die Spalte reinwerfen 

					reinwerfen(spalte - 42, spieler);  //spieler wirft den Stein in die ausgew�hlte Spalte
					ueberpruefung();                   //nachdem jemand reingeworfen hat �berpr�ft man ob jemand gewonnen hat
					
					if (spieler == 1) {           //wenn spieler eins ist dann kommt spieler 2 dran 
						spieler = 2;
					}
					else if (spieler == 2) {   //wenn spieler zwei  ist dann kommt spieler 1 dran 

						spieler = 1;
					}
				}
			}
			else {
				Sleep(100);
			}
		}
	}
	if (spieler == 1) {        //zeigt gewinner 
		statusText("Spieler 1 hat gewonnen!");
	}
	else {
		statusText("Spieler 2 hat gewonnen!");
	}

		
		getchar();

		return 0;
	}

