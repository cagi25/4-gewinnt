Unser Projekt heißt 4 gewinnt. Dabei handelt es sich um ein 2 Personen Strategiespiel. 
Dieses geht wie folgt: Jeder Spieler bekommt 21 Steine. Beispielsweise bekommt Spieler 1 rote Steine
und Spieler 2 gelbe. Die Spielsteine werden von den beiden Spielern abwechselnd in eine der Spalten
"geworfen". Wenn ein Spieler einen Spielstein in eine Spalte wirft, fällt dieser in die letzte unbesetzte
Zeile. Eine Herausforderung könnte sein, die Grenzen des Spiels technisch umzusetzten - z.B. Ein roter/gelber Stein darf
nicht einen gelben/roten Stein überschreiben. Weiterhin könnte die diagonale Plazierung eine Schwierigkeit darstellen.
Das Feld ist 7 Spalten und 6 Zeilen lang. Ist eine Spalte mit 6 Steinen gefüllt, 
so kann kein weiterer Stein "eingeworfen" werden. Wie der Name des Spiels schon verrät, gewinnt der Spieler,
der als erstes 4 gleichfarbige Steine vertikal, horizontal oder diagonal plaziert. Sind alle 42 Spielsteine gespielt 
und es hat kein Spieler geschafft vier zusammhängende Steine zu erreichen, so endet das Spiel unentschieden. 

Lösungsansatz:
Sobald das Feld besetzt ist, soll dieses "gesperrt" werden um nicht von einem anderen Stein überschrieben zu werden.